﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class BallSpwaner : MonoBehaviour
{
    public GameObject _ballPrefab;
    private List<Rigidbody2D> _balls;
    CollisionDetectionMode2D _mode = CollisionDetectionMode2D.Discrete;
    // Use this for initialization
    void Start()
    {
        if (_ballPrefab == null)
        {
            Debug.LogError("You forgot the ball prefab");
        }

        _balls = new List<Rigidbody2D>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButton("Ball++"))
        {
            for (int i = 0; i < 1; i++)
            {
                AddBall();
            }
        }
        if (Input.GetButton("Ball--"))
        {
            if (_balls.Count > 0)
            {
                DestroyImmediate(_balls[0].gameObject);
                _balls.RemoveAt(0);

            }
        }
        if (Input.GetButtonUp("Submit"))
        {
            if (_mode == CollisionDetectionMode2D.Discrete)
            {
                _mode = CollisionDetectionMode2D.Continuous;
                UIManager._instance.ModeText = _mode.ToString();

                foreach (Rigidbody2D b in _balls)
                {
                    b.collisionDetectionMode = _mode;
                }
            }
            else
            {
                _mode = CollisionDetectionMode2D.Discrete;
                UIManager._instance.ModeText = "Discrete";

                foreach (Rigidbody2D b in _balls)
                {
                    b.collisionDetectionMode = _mode;
                }
            }
        }
    }

    private void AddBall()
    {
        GameObject newBall = Instantiate(_ballPrefab, transform.position, Quaternion.identity) as GameObject;
       // GameObject newBall = Instantiate(_ballPrefab) as GameObject; ;

        if (newBall != null)
        {
            newBall.transform.parent = transform;
            Rigidbody2D rb2d = newBall.GetComponent<Rigidbody2D>();
            rb2d.collisionDetectionMode = _mode;
            rb2d.AddForce(new Vector2(Random.Range(-100, 100), 0));
            _balls.Add(rb2d);
        }
    }
}
