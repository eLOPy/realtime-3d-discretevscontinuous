﻿using UnityEngine;
using UnityEngine.UI;

using System.Collections;

public class UIManager : MonoBehaviour
{
    public static UIManager _instance;
    Text _Mode;
    // Use this for initialization
    void Start()
    {
        if (_instance == null)
        {
            _instance = this;
        }
        else
        {
            DestroyImmediate(this);
        }
        _Mode = GetComponentInChildren<Text>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public string ModeText { get { return _Mode.text; } set { _Mode.text = "Mode: " + value; } }
}
